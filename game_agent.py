"""This file contains all the classes you must complete for this project.

You can use the test cases in agent_test.py to help during development, and
augment the test suite with your own test cases to further test your code.

You must test your agent's strength against a set of agents with known
relative strength using tournament.py and include the results in your report.
"""
import random
import math



class Timeout(Exception):
    """Subclass base exception for code clarity."""
    pass

'''
    Define functions which determine the location of the players piece within
    the context of a board (i.e. is a player located at the edge of a board,
    the center of a board, as defined arbitrarily, or near the corner of the
    board )
'''
def isPositionWithinRect( rect_center, rect_width, rect_height, position ) :
    # Create the bounds of the box defined by the rect_center and sizing
    half_width = rect_width/2.0;
    half_height = rect_height/2.0
    min_x = rect_center[0] - half_width
    max_x = rect_center[0] + half_width
    
    min_y = rect_center[1] - half_height
    max_y = rect_center[1] + half_height
    
    # Check if position provided is within bounds of defined box.
    is_valid_x_pos = position[0] >= min_x and position[0] <= max_x
    is_valid_y_pos = position[1] >= min_y and position[1] <= max_y
    
    return is_valid_x_pos and is_valid_y_pos;
    

def isCenterPosition( width, height, player_loc ):
    # Reduce the width and height by half to the nearest integer
    width_x = math.ceil( width/2.0 )
    height_y = math.ceil( height/2.0 )
    
    # Find the center of the board, assuming (0, 0 ) is top left position
    center_y = height_y / 2.0
    center_x = width_x / 2.0
    center = (center_x, center_y)
    
    return isPositionWithinRect( center, width_x, height_y, player_loc )
    '''
    x_pos_within_bounds = \
        ( player_loc[0] <= center_x + 2 and player_loc[0] >= center_x - 2 );
    
    y_pos_within_bounds = \
            ( player_loc[1] <= center_y + 2 and player_loc[1] >= center_y - 2 );
    return x_pos_within_bounds and x_pos_within_bounds
    '''
    
def isEdgePosition( width, height, player_loc ) :   
    is_edge_pos = False
    if ( player_loc[0] == width or player_loc[0] == height ):
        is_edge_pos =  True;
    elif ( player_loc[1] == width or player_loc[1] == height ) :
        is_edge_pos = True;
    elif( player_loc[0] == 0 ) :
        is_edge_pos = True;
    elif( player_loc[1] == 0 ) :
        is_edge_pos = True;
    
    return is_edge_pos;
    
            
def isNearCornerPosition( width, height, player_loc ) :
    corner_positions = \
        [(0,0), (width, 0), (0, height), (width, height)]

    is_near_corner = False
    
    # Break if the player location is near any of the four corners. Assume 
    # an imaginery bounding box around the corner position with width and 
    # height 4, for ease of calculations. Assuming the player position 
    # will be a legal board location.
    for i, corner in enumerate( corner_positions ) :
        
        if ( isPositionWithinRect( corner, 4, 4, player_loc) ):
            is_near_corner = True;
            break; # done searching
    return is_near_corner;
    
    
def custom_score_rand( game, player ) :
    """The "random" evaluation function returns a random float between 0 and 
    1 if the player has neither won nor lost the game.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : hashable
        One of the objects registered by the game object as a valid player.
        (i.e., `player` should be either game.__player_1__ or
        game.__player_2__).

    Returns
    ----------
    float
        The heuristic value of the current game state
    """
    if game.is_loser(player):
        return float("-inf")
    
    if game.is_winner(player):
        return float("inf")
    
    score = random.random();
    return score;

def custom_score_improved( game, player ) :
    """The "Improved" evaluation function discussed in lecture that outputs a
    score equal to the difference in the number of moves available to the
    two players.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : hashable
        One of the objects registered by the game object as a valid player.
        (i.e., `player` should be either game.__player_1__ or
        game.__player_2__).

    Returns
    ----------
    float
        The heuristic value of the current game state
    """
    if game.is_loser(player):
        return float("-inf")

    if game.is_winner(player):
        return float("inf")

    own_moves = len(game.get_legal_moves(player))
    opp_moves = len(game.get_legal_moves(game.get_opponent(player)))
    return float(own_moves - opp_moves)


def custom_score_improved_mod( game, player ) :
    """Derivation of the "Improved" evaluation function discussed in 
    lecture that outputs a score equal to the difference in the number of 
    moves available to the two players. Weights moves in the beginning of the
    game that provide your player with more move options.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : hashable
        One of the objects registered by the game object as a valid player.
        (i.e., `player` should be either game.__player_1__ or
        game.__player_2__).

    Returns
    ----------
    float
        The heuristic value of the current game state
    """
    if game.is_loser(player):
        return float("-inf")

    if game.is_winner(player):
        return float("inf")

    own_moves = len(game.get_legal_moves(player))
    opp_moves = len(game.get_legal_moves(game.get_opponent(player)))
    
    # Favor moves in the beginning that maximize your potential moves over
    # your opponents. This is done by weighting your opponents number of 
    # moves initially.
    if ( game.move_count < 5):
        return float(own_moves - 1.5*opp_moves)
    else :
        return float( own_moves - opp_moves )
  
def custom_score_improved_mod2( game, player ) :
    """Derivation of the "Improved" evaluation function discussed in 
    lecture that outputs a score equal to the difference in the number of 
    moves available to the two players. Weights the players score by the 
    strength of the players location.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : hashable
        One of the objects registered by the game object as a valid player.
        (i.e., `player` should be either game.__player_1__ or
        game.__player_2__).

    Returns
    ----------
    float
        The heuristic value of the current game state
    """
    if game.is_loser(player):
        return float("-inf")

    if game.is_winner(player):
        return float("inf")
    
    # Get your players location and opponents location
    own_loc = game.get_player_location( player ) 
    opp_loc = game.get_player_location( game.get_opponent(player) )
    
    # Weight the player 
    locations = [own_loc, opp_loc]
    multipliers  = [ 1.0, 1.0]
    for i, player_loc in enumerate( locations) :
        if ( isCenterPosition( game.width, game.height, player_loc )) :
            multipliers[i] = 1.5
        elif ( isNearCornerPosition( game.width, game.height, player_loc )) :
            multipliers[i] = 0.8
        elif ( isEdgePosition( game.width, game.height, player_loc )) :
            multipliers[i] = 0.5
        
    own_moves = len(game.get_legal_moves(player))
    opp_moves = len(game.get_legal_moves(game.get_opponent(player)))
    
    return float( multipliers[0]*own_moves - multipliers[1]*opp_moves )
    

def custom_score_ave_over_children( game, player ) :
    """The "Average over children" evaluation function attempts to return 
    the average score of a nodes children. If there are no legal moves,
    the loss/win score is returned. Special handling is done for -inf and inf
    values if all of the children are not winning/losing moves.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : hashable
        One of the objects registered by the game object as a valid player.
        (i.e., `player` should be either game.__player_1__ or
        game.__player_2__).

    Returns
    ----------
    float
        The heuristic value of the current game state
    """
    legal_moves = game.get_legal_moves();
    
    # No legal moves, handle generically
    if ( not legal_moves ) :
        return custom_score_improved( game, player );
    
    # Loop over all of the children nodes
    scores = [] 
    for move in legal_moves :
        new_board = game.forecast_move( move );
        scores.append( custom_score_improved( new_board, player ) );
        
    # Handle the special situation where all of the child nodes 
    # are a winning move.
    all_scores_inf = True;
    for score in scores :
        if ( score != float('inf') ):
            all_scores_inf= False;
    if ( all_scores_inf ) :
        return float("-inf")
    
    # Handle the special situation where all of the child nodes 
    # are a losing move.
    all_scores_ninf = True;
    for score in scores :
        if ( score != float('inf') ):
            all_scores_ninf= False;
    if ( all_scores_ninf ) :
        return float("-inf")
    
    # Convert the -inf and +inf scores into -100 and +100
    # This is done to skew the score toward the win, while providiing a 
    # reasonable value that can be incorporated into the average.
    for i, score in enumerate( scores ) :
        if ( score == float('inf')) :
            scores[i] = 100.0
        elif ( score == float('-inf')) :
            scores[i] = -100.0
            
    # Average the scores over the children and return.
    length = len( scores )
    return sum( scores )/ length;
                      
        
    
def custom_score(game, player):
    """Calculate the heuristic value of a game state from the point of view
    of the given player.

    Note: this function should be called from within a Player instance as
    `self.score()` -- you should not need to call this function directly.

    Parameters
    ----------
    game : `isolation.Board`
        An instance of `isolation.Board` encoding the current state of the
        game (e.g., player locations and blocked cells).

    player : object
        A player instance in the current game (i.e., an object corresponding to
        one of the player objects `game.__player_1__` or `game.__player_2__`.)

    Returns
    -------
    float
        The heuristic value of the current game state to the specified player.
    """

    ''' Heuristic Ideas:
    - test the depth ( more depth == more quality answer)
    - random score between -inf and inf
    - number of empty spaces near me versus number of empty spaces near oponent
    - create a neural network that takes in player state, opponent state and compares to 
        game value, and legal moves
    - create a neural network that takes in board state and outputs value of game
    - play with looking at the previous number of moves to current
    - go down one more depth and average the eval functions for those nodes
    - tally up the number of ways your opponent can win by doing one depth look ahead
         
    '''
    
    return custom_score_improved_mod2( game, player );    
    
    
class CustomPlayer:
    """Game-playing agent that chooses a move using your evaluation function
    and a depth-limited minimax algorithm with alpha-beta pruning. You must
    finish and test this player to make sure it properly uses minimax and
    alpha-beta to return a good move before the search time limit expires.

    Parameters
    ----------
    search_depth : int (optional)
        A strictly positive integer (i.e., 1, 2, 3,...) for the number of
        layers in the game tree to explore for fixed-depth search. (i.e., a
        depth of one (1) would only explore the immediate sucessors of the
        current state.)

    score_fn : callable (optional)
        A function to use for heuristic evaluation of game states.

    iterative : boolean (optional)
        Flag indicating whether to perform fixed-depth search (False) or
        iterative deepening search (True).

    method : {'minimax', 'alphabeta'} (optional)
        The name of the search method to use in get_move().

    timeout : float (optional)
        Time remaining (in milliseconds) when search is aborted. Should be a
        positive value large enough to allow the function to return before the
        timer expires.
    """

    def __init__(self, search_depth=3, score_fn=custom_score,
                 iterative=True, method='minimax', timeout=10.):
        self.search_depth = search_depth
        self.iterative = iterative
        self.score = score_fn
        self.method = method
        self.time_left = None
        self.TIMER_THRESHOLD = timeout

    def timeout(self) :
        return self.time_left() < self.TIMER_THRESHOLD
    
    def get_move(self, game, legal_moves, time_left):
        """Search for the best move from the available legal moves and return a
        result before the time limit expires.

        This function must perform iterative deepening if self.iterative=True,
        and it must use the search method (minimax or alphabeta) corresponding
        to the self.method value.

        **********************************************************************
        NOTE: If time_left < 0 when this function returns, the agent will
              forfeit the game due to timeout. You must return _before_ the
              timer reaches 0.
        **********************************************************************

        Parameters
        ----------
        game : `isolation.Board`
            An instance of `isolation.Board` encoding the current state of the
            game (e.g., player locations and blocked cells).

        legal_moves : list<(int, int)>
            A list containing legal moves. Moves are encoded as tuples of pairs
            of ints defining the next (row, col) for the agent to occupy.

        time_left : callable
            A function that returns the number of milliseconds left in the
            current turn. Returning with any less than 0 ms remaining forfeits
            the game.

        Returns
        -------
        (int, int)
            Board coordinates corresponding to a legal move; may return
            (-1, -1) if there are no available legal moves.
        """

        self.time_left = time_left

        # Perform any required initializations, including selecting an initial
        # move from the game board (i.e., an opening book), or returning
        # immediately if there are no legal moves
        
        if ( not legal_moves ) :
            return ( -1, -1 );
        
        # Initialize the best move to the first legal move.
        best_move = legal_moves[0];
        
        # Initialize the best score the "Maximize" player has currently seen
        # to its worst possible outcome.
        best_score = float( '-inf' );
        
        try:
            # Initialize the variables for depth fixed search.
            # The depth is set to the cutoff depth minus 1 (it will be 
            # immediately incremented in the logic loop.)
            depth_cutoff = self.search_depth;
            depth = self.search_depth - 1
            if ( self.iterative ) :
                depth = -1; # start from the beginning
                depth_cutoff = 1e4 

            while ( depth < depth_cutoff ) :
                depth = depth + 1;
                if self.method == 'alphabeta' :
                    best_score, best_move = self.alphabeta( game, depth, True );
                else  :
                    best_score, best_move = self.minimax( game, depth, True );
                    
                if (best_score == float( 'inf')) :
                    break; # The game has ended.
                        
                
            # The search method call (alpha beta or minimax) should happen in
            # here in order to avoid timeout. The try/except block will
            # automatically catch the exception raised by the search method
            # when the timer gets close to expiring
        except Timeout:
            # Return the best move seen so far.
            return best_move;

        # Return the best move from the last completed search iteration
        return best_move;

    def minimax(self, game, depth, maximizing_player=True):
        """Implement the minimax search algorithm as described in the lectures.

        Parameters
        ----------
        game : isolation.Board
            An instance of the Isolation game `Board` class representing the
            current game state

        depth : int
            Depth is an integer representing the maximum number of plies to
            search in the game tree before aborting

        maximizing_player : bool
            Flag indicating whether the current search depth corresponds to a
            maximizing layer (True) or a minimizing layer (False)

        Returns
        -------
        float
            The score for the current search branch

        tuple(int, int)
            The best move for the current branch; (-1, -1) for no legal moves

        Notes
        -----
            (1) You MUST use the `self.score()` method for board evaluation
                to pass the project unit tests; you cannot call any other
                evaluation function directly.
        """
        if self.time_left() < self.TIMER_THRESHOLD:
            raise Timeout()

        if (depth == 0 or (not game.get_legal_moves()) ) : # End of search
            return self.score( game, self ), (-1,-1);
        
        best_move = (-1,1)
        legal_moves = game.get_legal_moves();
        new_depth = depth - 1;
        if ( maximizing_player ) :
            best_value = float('-inf')
            for move in legal_moves :
                # Creates a deep copy of the game board with the provided move 
                # applied.
                new_game_board = game.forecast_move( move );
                value, _ = self.minimax( new_game_board, new_depth, False )
                if ( value > best_value ) :
                    best_value = value;
                    best_move = move;
            return best_value, best_move;
            
        else : # minimizing player
            best_value = float('inf')
            for move in legal_moves :
                # Creates a deep copy of the game board with the provided move 
                # applied.
                new_game_board = game.forecast_move( move );
                value, _ = self.minimax( new_game_board, new_depth, True )
                if ( value < best_value ) :
                    best_value = value;
                    best_move = move;
        
            return best_value, best_move;

    def alphabeta(self, game, depth, alpha=float("-inf"), beta=float("inf"), maximizing_player=True):
        """Implement minimax search with alpha-beta pruning as described in the
        lectures.

        Parameters
        ----------
        game : isolation.Board
            An instance of the Isolation game `Board` class representing the
            current game state

        depth : int
            Depth is an integer representing the maximum number of plies to
            search in the game tree before aborting

        alpha : float
            Alpha limits the lower bound of search on minimizing layers

        beta : float
            Beta limits the upper bound of search on maximizing layers

        maximizing_player : bool
            Flag indicating whether the current search depth corresponds to a
            maximizing layer (True) or a minimizing layer (False)

        Returns
        -------
        float
            The score for the current search branch

        tuple(int, int)
            The best move for the current branch; (-1, -1) for no legal moves

        Notes
        -----
            (1) You MUST use the `self.score()` method for board evaluation
                to pass the project unit tests; you cannot call any other
                evaluation function directly.
        """
        if self.time_left() < self.TIMER_THRESHOLD:
            raise Timeout()

        if (depth == 0 or (not game.get_legal_moves()) ) : # End of search
            return self.score( game, self ), (-1,-1);
        
        best_move = (-1,1)
        legal_moves = game.get_legal_moves();
        new_depth = depth - 1;
        if ( maximizing_player ) :
            best_value = float('-inf')
            for move in legal_moves :
                # Creates a deep copy of the game board with the provided move 
                # applied.
                new_game_board = game.forecast_move( move );
                value, _ = self.alphabeta( 
                        new_game_board, new_depth,alpha, beta, False )
                if ( value > best_value ) :
                    best_value = value;
                    alpha = best_value;
                    best_move = move;
                if ( best_value >= beta ) :
                    break;
            return best_value, best_move;
            
        else : # minimizing player
            best_value = float('inf')
            for move in legal_moves :
                # Creates a deep copy of the game board with the provided move 
                # applied.
                new_game_board = game.forecast_move( move );
                value, _ = self.alphabeta( 
                        new_game_board, new_depth, alpha, beta, True )
                if ( value < best_value ) :
                    best_value = value;
                    beta = best_value;
                    best_move = move;
                if ( best_value <= alpha ) :
                    break;
            return best_value, best_move;
